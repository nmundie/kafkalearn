package com.kafkalearn.service;

import com.kafkalearn.models.User;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;

import java.util.concurrent.ExecutionException;

@Service
public class KafkaService {

    private final KafkaTemplate<String, User> kafkaTemplate;

    public KafkaService(KafkaTemplate<String, User> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void send(User user) throws ExecutionException, InterruptedException {

        ListenableFuture<SendResult<String, User>> resultListenableFuture = kafkaTemplate.send("kafkalearn", user);
        SendResult<String, User>  sendResult = resultListenableFuture.get();
        System.out.println("Partition message was sent to is " +sendResult.getRecordMetadata().partition());
    }
}
