package com.kafkalearn;

import com.kafkalearn.models.User;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.KafkaListener;

@SpringBootApplication
public class KafkalearnApplication {

    public static void main(String[] args) {
        SpringApplication.run(KafkalearnApplication.class, args);
    }


    @KafkaListener(id = "myId", topics = "kafkalearn")
    public void listen(User user) {
        System.out.println(user.toString());
    }


}
