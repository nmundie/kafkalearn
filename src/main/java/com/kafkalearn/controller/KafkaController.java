package com.kafkalearn.controller;

import com.kafkalearn.models.User;
import com.kafkalearn.service.KafkaService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutionException;

@RestController
public class KafkaController {

    private final KafkaService kafkaService;

    public KafkaController(KafkaService kafkaService) {
        this.kafkaService = kafkaService;
    }
    @PostMapping(path="/v1/createuser")
    public ResponseEntity<String> AddUser(@RequestBody User user) throws ExecutionException, InterruptedException {
        // Add user object to Kafka topic
        kafkaService.send(user);

        return new ResponseEntity<>(HttpStatus.OK);
    }

}
