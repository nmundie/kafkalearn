package com.kafkalearn.models;

import lombok.Data;

@Data
public class User {
    private String email;
    private String firstName;
    private String lastName;
    private Integer age;
}
